import 'package:analysis_utils/analysis.dart';
import 'package:test/test.dart';

String _source = '''library lib.name;
import "package:path/path.dart" as p hide equals;
export './analysis_utils_test.dart' show ChildClass;
part 'part.dart';
///docs
@ClassUno
var var1;
@ClassUno
@ClassDos
///docs
List<int> var2 = [4, 3, 2, 1];
@ClassUno
String var3 = "sorp", var4 = "longa";

class ClassUno{
  int field1;
  String field2 = "default2";

  String method1() => field2 + field1.toString();
}

class ClassDos{
  @ClassUno
  final List<String> field1;
  ClassDos(this.field1, String simple, {List<int> named: [2]});

  void method1(int arg1, [arg2, String arg3 = "sorp"]){}
}''';

main() {
  SourceAnalysis sourceAnalysis = SourceAnalysis.forContents(_source);
  ClassAnalysis c1 = sourceAnalysis.classes
          .singleWhere((ClassAnalysis c) => c.name == "ClassUno", orElse: null),
      c2 = sourceAnalysis.classes
          .singleWhere((ClassAnalysis c) => c.name == "ClassDos", orElse: null);
  TopLevelVarAnalysis v1 = sourceAnalysis.topLevelVariables.first,
      v2 = sourceAnalysis.topLevelVariables[1],
      v3 = sourceAnalysis.topLevelVariables[2],
      v4 = sourceAnalysis.topLevelVariables.last;
  LibraryDirectiveAnalysis libraryDirective = sourceAnalysis.library!;
  ImportDirectiveAnalysis importDirective = sourceAnalysis.imports.single;
  ExportDirectiveAnalysis exportDirective = sourceAnalysis.exports.single;
  PartDirectiveAnalysis partDirective = sourceAnalysis.parts.single;
  group("SourceAnalysis:", () {
    group("maps", () {
      Map mapa = sourceAnalysis.toMap();
      test("topLevelVariables", () {
        expect(mapa["topLevelVariables"], isNotEmpty);
        expect(mapa["topLevelVariables"].length, equals(4));
        expect(mapa["topLevelVariables"][0]["docs"], equals("docs"));
        expect(mapa["topLevelVariables"][0]["metadata"][0]["name"],
            equals("ClassUno"));
        // expect(mapa["topLevelVariables"], );
      });
      test("class", () {
        expect(mapa["classes"], isNotNull);
        expect(mapa["classes"][1]["fields"], isNotEmpty);
        expect(
            mapa["classes"][1]["fields"]["field1"]["name"], equals("field1"));
        expect(mapa["classes"][1]["fields"]["field1"]["type"],
            equals("List<String>"));
        //void method1(int arg1, [arg2, String arg3 = "sorp"]){}
        expect(mapa["classes"][1]["methods"], isNotEmpty);
        expect(mapa["classes"][1]["methods"]["method1"]["name"],
            equals("method1"));
        expect(mapa["classes"][1]["methods"]["method1"]["parameters"],
            hasLength(3));
        expect(
            mapa["classes"][1]["methods"]["method1"]["parameters"][0]["name"],
            equals("arg1"));
        expect(
            mapa["classes"][1]["methods"]["method1"]["parameters"][1]["name"],
            equals("arg2"));
        expect(
            mapa["classes"][1]["methods"]["method1"]["parameters"][2]["name"],
            equals("arg3"));
      });
    });
    group("SourceLocations:", () {
      test('Directives', () {
        expect(libraryDirective.location.start.line, equals(0));
        expect(libraryDirective.location.start.offset, equals(0));
        expect(libraryDirective.location.text, equals("library lib.name;"));
        expect(libraryDirective.location.end.offset,
            equals(libraryDirective.location.length));
        expect(libraryDirective.location.end.line, equals(0));
        expect(importDirective.location.start.offset,
            equals(libraryDirective.location.length + 1));
        // expect(importDirective.location.start.line, equals(1));
        expect(importDirective.location.text,
            equals('import "package:path/path.dart" as p hide equals;'));
        expect(
            importDirective.location.end.offset,
            equals(libraryDirective.location.length +
                importDirective.location.length +
                1));
        expect(exportDirective.location.start.offset,
            equals(importDirective.location.end.offset + 1));
        // expect(exportDirective.location.start.line, equals(2));
        expect(exportDirective.location.text,
            equals("export './analysis_utils_test.dart' show ChildClass;"));
        expect(partDirective.location.start.offset,
            equals(exportDirective.location.end.offset + 1));
        // expect(partDirective.location.start.line, equals(3));
        expect(partDirective.location.text, equals("part 'part.dart';"));
      });
      test("TopLevelVariables", () {
        expect(v1.location.start.offset,
            equals(sourceAnalysis.code.indexOf("///docs")));
        expect(v1.location.text, equals("///docs\n@ClassUno\nvar var1;"));
        expect(
            v2.location.text,
            equals(
                "@ClassUno\n@ClassDos\n///docs\nList<int> var2 = [4, 3, 2, 1];"));
        expect(v3.location.text, startsWith("@ClassUno"));
        expect(v4.location.text, startsWith("@ClassUno"));
        expect(
            _source.substring(v1.location.start.offset, v1.location.end.offset),
            equals(v1.location.text));
        expect(
            _source.substring(v3.location.start.offset, v3.location.end.offset),
            equals(v3.location.text));
      });
      test("Classes", () {
        expect(c1.location.text, startsWith("class ClassUno{"));
        expect(c1.location.text, endsWith(".toString();\n}"));
        expect(c2.location.text, startsWith("class ClassDos{"));
        expect(c2.location.text, endsWith("){}\n}"));
        expect(
            _source.substring(c1.location.start.offset, c1.location.end.offset),
            equals(c1.location.text));
        expect(
            _source.substring(c2.location.start.offset, c2.location.end.offset),
            equals(c2.location.text));
      });
    });
    group("Directives:", () {
      test('library', () {
        expect(libraryDirective.name, equals("lib.name"));
        expect(libraryDirective.libraryIdentifier?.name,
            equals(libraryDirective.name));
      });
      test("import", () {
        expect(importDirective.uri, equals("package:path/path.dart"));
        expect(importDirective.prefix, equals("p"));
        expect(importDirective.hides.single, equals("equals"));
        expect(importDirective.shows, isEmpty);
      });
      test("export", () {
        expect(exportDirective.uri, equals("./analysis_utils_test.dart"));
        expect(exportDirective.shows.single, equals("ChildClass"));
        expect(exportDirective.hides, isEmpty);
      });
      test("part", () {
        expect(partDirective.uri, equals("part.dart"));
      });
    });
    group("TopLevelVariables:", () {
      test("names", () {
        expect(v1.name, equals("var1"));
        expect(v2.name, equals("var2"));
        expect(v3.name, equals("var3"));
        expect(v4.name, equals("var4"));
      });
      test("default values", () {
        expect(v1.defaultValue, isNull);
        expect(v2.defaultValue, equals([4, 3, 2, 1]));
        expect(v3.defaultValue, equals("sorp"));
        expect(v4.defaultValue, equals("longa"));
      });
      test("types", () {
        expect(v1.typeString, equals("var"));
        expect(v2.typeString, equals("List<int>"));
        expect(v3.typeString, equals("String"));
        expect(v4.typeString, equals("String"));
      });
      test("annotations & docs", () {
        expect(v1.docs, equals("docs"));
        expect(v2.docs, equals("docs"));
        expect(v3.docs, isEmpty);
        expect(v4.docs, isEmpty);
        expect(v1.metadata, isNotEmpty);
        expect(v1.metadata.length, equals(1));
        expect(v2.metadata, isNotEmpty);
        expect(v2.metadata.length, equals(2));
        expect(v3.metadata.single.name, equals("ClassUno"));
        expect(v4.metadata.single.name, equals("ClassUno"));
      });
    });
    group("Classes:", () {
      test("2 classes present", () {
        expect(sourceAnalysis.classes.length, equals(2));
        expect(c1, isNotNull);
        expect(c2, isNotNull);
      });
      test("fields", () {
        expect(c1.fields.keys, containsAll(["field1", "field2"]));
        expect(c2.fields.keys, contains("field1"));
        expect(c1.fields["field1"]!.type.toString(), equals("int"));
        expect(c1.fields["field2"]!.type.toString(), equals("String"));
        expect(c1.fields["field1"]?.defaultValue, isNull);
        expect(c1.fields["field2"]!.defaultValue, equals("default2"));
        expect(c2.fields["field1"]!.type.toString(), equals("List<String>"));
      });
      test("constructors", () {
        expect(c1.constructors.values, isEmpty);
        expect(c2.constructors.values.single.parameters?.required.first.name,
            equals("field1"));
        expect(
            c2.constructors.values.single.parameters?.required.first
                .isThisInitializer,
            isTrue);
        expect(
            c2.constructors.values.single.parameters?.required.first
                .defaultValue,
            isNull);
        expect(
            c2.constructors.values.single.parameters?.required.first.typeString,
            isNull);
        expect(c2.constructors.values.single.parameters?.required.last.name,
            equals("simple"));
        expect(
            c2.constructors.values.single.parameters?.required.last.typeString,
            equals("String"));
        expect(
            c2.constructors.values.single.parameters?.required.last
                .defaultValue,
            isNull);
        expect(c2.constructors.values.single.parameters?.named.single.name,
            equals("named"));
        expect(
            c2.constructors.values.single.parameters?.named.single.typeString,
            equals("List<int>"));
        expect(
            c2.constructors.values.single.parameters?.named.single.defaultValue,
            equals([2]));
      });
      test("methods", () {
        // String method1 => field2 + field1.toString();
        // void method1(int arg1, [arg2, String arg3 = "sorp"]){}
        expect(c1.methods.values.first.name, equals("method1"));
        expect(c1.methods.values.first.parameters?.name, isNull);
        expect(c1.methods.values.first.parameters?.all, isEmpty);
        expect(c2.methods.values.first.name, equals("method1"));
        expect(
            c2.methods.values.first.parameters?.positionalOptionals.first.name,
            equals("arg2"));
        expect(
            c2.methods.values.first.parameters?.positionalOptionals.first
                .defaultValue,
            isNull);
        expect(
            c2.methods.values.first.parameters?.positionalOptionals.first
                .typeString,
            isNull);
        expect(
            c2.methods.values.first.parameters?.positionalOptionals.last
                .typeString,
            equals("String"));
        expect(
            c2.methods.values.first.parameters?.positionalOptionals.last.name,
            equals("arg3"));
        expect(
            c2.methods.values.first.parameters?.positionalOptionals.last
                .defaultValue,
            equals("sorp"));
        expect(c2.methods.values.first.parameters?.required.first.typeString,
            equals("int"));
        expect(c2.methods.values.first.parameters?.required.first.name,
            equals("arg1"));
        expect(c2.methods.values.first.parameters?.required.first.defaultValue,
            isNull);
      });
    });
  }, testOn: "vm");
}
