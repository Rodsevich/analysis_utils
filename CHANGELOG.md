# Changelog

## 0.1.1

- Eliminiación de lo respectivo a los mirrors

## 0.0.11

- Cambio de version de analyzer
- Adaptación de las APIs de analisus de files en su propia libraria (analyzer_components)

## 0.0.10

- Agregado de analíticas de directivas en el SourceAnalysis

## 0.0.9

- Added metadata, sourceLocations and docs functionality and testings for TopLevelVars

## 0.0.8

- Added capacity of statically analyze only with analyzer (mirrors free)

## 0.0.6

- Mocosoft free: repo en Gitlab

## 0.0.5

- Add Uri resolving capabilities
- Separated the code attribute in a getter (for "lazy loading")

## 0.0.4

- isPrivate to MethodAnalysis

## 0.0.3

- Boludeces para que ande, desestimar...

## 0.0.2

- Arreglo de links en el readme
- Dejar presentable y en inglés el código de ejemplo

## 0.0.1

- Initial version, created by Stagehand
