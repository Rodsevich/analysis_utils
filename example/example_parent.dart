import 'example_annotation.dart';

class ParentClass {
  @annon(1, text: "sorp")
  @annon(2)
  @annon(3, text: "longa")
  @annon(4)
  List<int> sorp = [];

  double var1 = 2.0, initVal = 723 + 123 / 2;
}
