//custom annotation, ain't a documentation comment, shouldn't be parsed, so
class annon {
  final int n;
  final String text;
  const annon(this.n, {this.text = 'text'});
}
