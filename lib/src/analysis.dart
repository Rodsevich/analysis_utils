import 'dart:io';
import "dart:mirrors" hide SourceLocation;
import 'package:analysis_utils/analyzer_components.dart';
import 'package:analysis_utils/src/expression_handler.dart';
import 'package:analysis_utils/src/source_span.dart';
import 'package:analyzer/dart/ast/ast.dart';
import 'package:analyzer/src/dart/ast/constant_evaluator.dart';
import 'package:analyzer/dart/ast/token.dart';
import 'package:analyzer/dart/ast/visitor.dart';
import 'package:source_span/source_span.dart';

/// An analyzed Dart file/code
///
/// All what must be parsed with the analysis package should be done with this
class SourceAnalysis {
  /// Saves a cache that matches raw source code to SourceAnalysis
  static final Map<String, SourceAnalysis> _analysisCache = {};

  ///Saves a cache for naming the fictional paths of raw provided code
  static int _pathsCacheIndex = 1;

  /// The path in a String manner
  late String path;

  /// The analysis of the AST Structure of the source
  late CompilationUnit compilationUnit;

  late String _code;

  List<ClassAnalysis> _classes = [];
  List<EnumAnalysis> _enums = [];
  List<MixinAnalysis> _mixins = [];
  List<TopLevelVarAnalysis> _topLevelVariables = [];
  List<DirectiveAnalysis> _directives = [];

  /// The code of the file
  String get code {
    if (_code == null) {
      File file = File(path);
      _code = file.readAsStringSync();
    }
    return _code;
  }

  /// Entrypoint for static code. You can use it to analyze the declarations (TopLevelVariables
  /// and Classes) of the dart code provided in `contents`
  factory SourceAnalysis.forContents(String contents) {
    if (_analysisCache.containsKey(contents))
      return _analysisCache[contents]!;
    else {
      final SourceAnalysis ret = SourceAnalysis._forContents(contents);
      _analysisCache[contents] = ret;
      return ret;
    }
  }

  factory SourceAnalysis.forFilePath(String path) {
    if (_analysisCache.containsKey(path))
      return _analysisCache[path]!;
    else {
      final SourceAnalysis ret = SourceAnalysis._(path);
      _analysisCache[path] = ret;
      return ret;
    }
  }

  // factory SourceAnalysis.forMirror(DeclarationMirror mirror) {
  //   Uri uri = mirror.location?.sourceUri;
  //   if (uri == null)
  //     throw UnsupportedError(
  //         "The mirror '$mirror' doesn't support the sourceUri property. Try providing a SourceAnalysis instance from another mirror that does.");
  //   String path = resolvePath(uri);
  //   return SourceAnalysis.forFilePath(path);
  // }

  SourceAnalysis._(this.path) {
    if (path.endsWith(".dart")) {
      this.compilationUnit = getCompilationUnitForPath(path);
    } else
      throw Exception("You can only parse .dart files");
  }

  SourceAnalysis._forContents(String contents) {
    this._code = contents;
    this.path = "path${_pathsCacheIndex++}.dart";
    this.compilationUnit =
        getCompilationUnitForSource(contents); //, path: path);
  }

  List<DirectiveAnalysis> get directives {
    if (_directives != null) {
      return _directives;
    } else {
      _directives = [];
      compilationUnit.directives.forEach((d) {
        final directive = (d is ImportDirective)
            ? ImportDirectiveAnalysis(d, this)
            : (d is ExportDirective)
                ? ExportDirectiveAnalysis(d, this)
                : (d is LibraryDirective)
                    ? LibraryDirectiveAnalysis(d, this)
                    : (d is PartDirective)
                        ? PartDirectiveAnalysis(d, this)
                        : (d is PartOfDirective)
                            ? PartOfDirectiveAnalysis(d, this)
                            : throw UnsupportedError(
                                "What the heck is a ${d.runtimeType} directive??");
        _directives.add(directive as DirectiveAnalysis);
      });
      return _directives;
    }
  }

  LibraryDirectiveAnalysis? get library => directives.singleWhere(
      (DirectiveAnalysis d) => d is LibraryDirectiveAnalysis,
      orElse: null) as LibraryDirectiveAnalysis;
  List<ImportDirectiveAnalysis> get imports =>
      directives.whereType<ImportDirectiveAnalysis>().toList();
  List<ExportDirectiveAnalysis> get exports =>
      directives.whereType<ExportDirectiveAnalysis>().toList();
  List<PartOfDirectiveAnalysis> get partsOf =>
      directives.whereType<PartOfDirectiveAnalysis>().toList();
  List<PartDirectiveAnalysis> get parts =>
      directives.whereType<PartDirectiveAnalysis>().toList();

  List<ClassAnalysis> get classes {
    if (_classes != null)
      return _classes;
    else {
      _classes = [];
      compilationUnit.declarations
          .whereType<ClassDeclaration>()
          .forEach((ClassDeclaration classDeclaration) {
        ClassAnalysis analysis =
            ClassAnalysis.fromAnalysis(classDeclaration, source: this);
        _classes.add(analysis);
      });
      return _classes;
    }
  }

  List<MixinAnalysis> get mixins {
    if (_mixins != null) {
      return _mixins;
    } else {
      _mixins = [];
      compilationUnit.declarations
          .whereType<MixinDeclaration>()
          .forEach((MixinDeclaration m) {
        MixinAnalysis analysis = MixinAnalysis.fromAnalysis(m, source: this);
        _mixins.add(analysis);
      });
      return _mixins;
    }
  }

  List<TopLevelVarAnalysis> get topLevelVariables {
    if (_topLevelVariables != null)
      return _topLevelVariables;
    else {
      _topLevelVariables = [];
      compilationUnit.declarations
          .whereType<TopLevelVariableDeclaration>()
          .forEach((TopLevelVariableDeclaration tlv) {
        tlv.variables.variables.forEach((VariableDeclaration v) {
          TopLevelVarAnalysis analysis =
              TopLevelVarAnalysis.fromAnalysis(tlv, v, this);
          _topLevelVariables.add(analysis);
        });
      });
      return _topLevelVariables;
    }
  }

  List<EnumAnalysis> get enums {
    if (_enums != null)
      return _enums;
    else {
      _enums = [];
      compilationUnit.declarations
          .whereType<EnumDeclaration>()
          .forEach((EnumDeclaration e) {
        _enums.add(EnumAnalysis.fromAnalysis(e, sourceAnalysis: this));
      });
    }
    return _enums;
  }

  Map toMap() => {
        "classes": this.classes.map((c) => c.toMap()).toList(),
        // mixins: this.mixins.map((c) => c.toMap()),
        "topLevelVariables":
            this.topLevelVariables.map((c) => c.toMap()).toList(),
        "directives": {
          "library": this.library.toString(),
          "imports": this.imports.map((d) => d.toString()).toList(),
          "exports": this.exports.map((d) => d.toString()).toList(),
          "parts": this.parts.map((d) => d.toString()).toList(),
          "partsof": this.partsOf.map((d) => d.toString()).toList()
        }
      };
}

/// base class used for traversing Analysis nodes in oprder to find the needed
/// ones (allocated in `found`)
abstract class Finder<D> extends SimpleAstVisitor {
  final String name;
  D? found;
  Finder(this.name);
}

class ParameterFinder extends Finder<FormalParameterList> {
  ParameterFinder(String name) : super(name) {}

  // @override
  // visitImportDirective(ImportDirective node) => null;
  // @override
  // visitComment(node) => null;
  // @override
  // visitTypeName(node) => print("TypeName: $node");
  // @override
  // visitSimpleIdentifier(node) => print("SimpleIdentifier: $node");
  // @override
  visitFormalParameterList(FormalParameterList node) {
    if (found != null) throw Exception("There can't be 2 parametersList, WTF?");
    this.found = node;
  }
  //
  // @override
  // visitExpressionFunctionBody(node) => print("FunctionBody: $node");
}

class ClassFinder extends Finder<ClassDeclaration> {
  ClassFinder(String name) : super(name);

  @override
  visitClassDeclaration(ClassDeclaration node) {
    if (node.name.toString() == name) {
      this.found = node;
    }
    return super.visitClassDeclaration(node);
  }
}

class FieldFinder extends Finder<FieldDeclaration> {
  FieldFinder(String name) : super(name);
  @override
  visitFieldDeclaration(FieldDeclaration node) {
    if (node.fields.variables
        .any((VariableDeclaration v) => v.name.toString() == name)) {
      this.found = node;
    }
    return super.visitFieldDeclaration(node);
  }
}

class ConstructorFinder extends Finder<ConstructorDeclaration> {
  ConstructorFinder(String name)
      : super(name.startsWith(".") ? name.substring(1) : name);
  visitConstructorDeclaration(ConstructorDeclaration node) {
    if ((node.name?.toString() ?? "") == name) {
      this.found = node;
    }
    return super.visitConstructorDeclaration(node);
  }
}

class MethodFinder extends Finder<MethodDeclaration> {
  MethodFinder(String name) : super(name);
  @override
  visitMethodDeclaration(MethodDeclaration node) {
    if (node.name.toString() == name) this.found = node;
    return super.visitMethodDeclaration(node);
  }
}

class TopLevelVariableFinder extends Finder<TopLevelVariableDeclaration> {
  TopLevelVariableFinder(String name) : super(name);
  @override
  visitTopLevelVariableDeclaration(TopLevelVariableDeclaration node) {
    if (node.variables.variables
        .any((VariableDeclaration d) => d.name.value() == this.name)) {
      this.found = node;
      return super.visitTopLevelVariableDeclaration(node);
    }
  }
}

abstract class EntityAnalysis<A extends AstNode, F extends Finder> {
// abstract class EntityAnalysis<A extends AstNode, F extends Finder,
//     M extends DeclarationMirror> {
  /// The name which identifies this entity, whichever the type of it
  late String name;

  /// The documentation block that accompanies the entity in where it's
  /// declared in the source
  String docs = '';

  /// The location in the source code that contains this entity
  late SourceSpan location;

  /// The list of annotations that accompanies this entity
  List<MetadataAnalysis> metadata = [];

  /// The code source from where this entity comes
  SourceAnalysis source;

  // /// A mirror on the entity, it can be null if the analysis weren't started
  // /// from a mirror bias, (i.e. started from raw code analysis)
  // M mirror;

  /// An AST node representation from package:analyzer that holds the
  /// analyzed code that declares the entity. It can be null if there is none
  /// (e.g. a default constructor)
  A analyzerDeclaration;

  /// An ASTVisitor that is used to get the declaration that is searched for
  F? _entityFinder;

  /// the analyzer node tha contains this entity. For example a FieldDeclaration
  /// should be contained by a ClassDeclaration
  Declaration? _analyzerContainer;

  List<MetadataAnalysis> _computeMetadata() {
    NodeList<Annotation> annotations =
        (analyzerDeclaration as AnnotatedNode).metadata;
    List<MetadataAnalysis> ret = [];
    for (int i = 0; i < annotations.length; i++) {
      Annotation a = annotations[i];
      // if (mirror == null) {
      ret.add(MetadataAnalysis.fromAnalysis(a));
      // } else {
      //   ret.add(MetadataAnalysis(a, mirror.metadata[i]));
      // }
    }
    return ret;
  }

  EntityAnalysis.fromAnalysis(this.analyzerDeclaration, this.source) {
    // if (analyzerDeclaration == null)
    //   throw ArgumentError.notNull("analyzerDeclaration");
    _computeAnalysis();
    this.location = computeLocationFromNode(this.analyzerDeclaration, source);
  }

  // EntityAnalysis(this.mirror, {Declaration analyzerContainer, this.source}) {
  //   if (mirror == null) throw ArgumentError.notNull("mirror");
  //   this._analyzerContainer = analyzerContainer;
  //   _computeName();
  //   this.source ??= SourceAnalysis.forMirror(mirror);
  //   _findDeclaration();
  //   _computeAnalysis();
  //   this.location = computeLocationFromMirror(mirror);
  // }

  void _computeName() {
    // this.name = MirrorSystem.getName(mirror.simpleName);
    //remove superclasses and mixins from name
    if (name != null) {
      int index = name!.indexOf("&");
      if (index > 0) {
        name = name!.substring(name!.startsWith("_") ? 1 : 0, index);
      }
    }
  }

  String toString() => name ?? '';

  // _findDeclaration() {
  //   ClassMirror finderMirror = reflectClass(F);
  //   this._entityFinder = finderMirror.newInstance(Symbol(""), [name]).reflectee;
  //   if (_analyzerContainer != null)
  //     _analyzerContainer.visitChildren(_entityFinder);
  //   else
  //     source.compilationUnit.visitChildren(_entityFinder);
  //   this.analyzerDeclaration = _entityFinder.found as A;
  //   // if (analyzerDeclaration == null) {
  //   //   if (this is! ConstructorAnalysis)
  //   //     throw StateError(
  //   //         "Null analyzer declaration for this '${this.runtimeType}' analysis");
  //   // }
  // }

  _computeAnalysis() {
    if (analyzerDeclaration != null && analyzerDeclaration is AnnotatedNode) {
      this.docs = _computeDocs(analyzerDeclaration as AnnotatedNode);
      this.metadata = _computeMetadata();
    }
  }

  String _computeDocs(AnnotatedNode declaration) {
    List<Token> tokens = declaration.documentationComment?.tokens ?? [];
    if (tokens.isEmpty) return "";
    if (tokens.length == 1) {
      //Maybe a /** docs */ documentation type?
      String doc = tokens.first.toString();
      if (doc.startsWith("/**"))
        return _computeComplexDocsComment(doc);
      else if (doc.startsWith("///"))
        return _computeNormalDocsComment(tokens);
      else
        throw UnsupportedError(
            "Don't know what to do here. Please make me an issue in analysis_utils package showing the documentation of '$name' entity");
    } else {
      // Must be the classical /// documentation
      return _computeNormalDocsComment(tokens);
    }
  }

  String _computeComplexDocsComment(String docLine) {
    RegExp formatter = RegExp("\n[ *]*");
    return docLine
        .substring(3, docLine.length - 3) //removes the /** and */
        .replaceAll(formatter, " ")
        .trim();
  }

  String _computeNormalDocsComment(List<Token> tokens) {
    return tokens
        .map((Token t) => t.toString().substring(3).trim())
        .join(" ")
        .trim();
  }
}

class MetadataAnalysis {
  // Type type;
  late String name;
  Annotation node;
  // dynamic instance;
  // InstanceMirror mirror;
  ArgumentsResolution? arguments;

  // MetadataAnalysis(this.node, this.mirror) {
  //   this.instance = mirror.reflectee;
  //   this.type = mirror.type.reflectedType;
  //   _processGenerics();
  // }

  MetadataAnalysis.fromAnalysis(this.node) {
    _processGenerics();
  }

  void _processGenerics() {
    this.name = node.name.name;
    if (node.arguments != null)
      this.arguments = ArgumentsResolution.fromArgumentList(node.arguments!);
  }

  Map toMap() => {
        "name": this.name,
        "arguments": this.arguments?.all,
        "toString": this.toString()
      };

  String toString() => node.toString();
}

abstract class DirectiveAnalysis<T extends Directive> {
  final T directive;
  late SourceSpan location;
  DirectiveAnalysis(this.directive, SourceAnalysis source) {
    this.location = computeLocationFromNode(directive, source);
  }

  String toString() => this.directive.toString();
}

abstract class UriBasedDirectiveAnalysis<T extends UriBasedDirective>
    extends DirectiveAnalysis<T> {
  late String? uri;
  UriBasedDirectiveAnalysis(T directive, SourceAnalysis source)
      : super(directive, source) {
    this.uri = directive.uri.stringValue;
  }
}

abstract class NamespaceBasedDirectiveAnalysis<T extends NamespaceDirective>
    extends UriBasedDirectiveAnalysis<T> {
  List<String> shows = [], hides = [];
  NamespaceBasedDirectiveAnalysis(T directive, SourceAnalysis source)
      : super(directive, source) {
    directive.combinators.forEach((Combinator c) {
      if (c is HideCombinator) {
        c.hiddenNames.forEach((h) {
          hides.add(h.name);
        });
      } else if (c is ShowCombinator) {
        c.shownNames.forEach((s) {
          shows.add(s.name);
        });
      }
    });
  }
}

abstract class LibraryBasedDirectiveAnalysis<T extends Directive>
    extends DirectiveAnalysis<T> {
  final LibraryIdentifier? libraryIdentifier;
  LibraryBasedDirectiveAnalysis(
      T directive, SourceAnalysis source, this.libraryIdentifier)
      : super(directive, source);
  String? get name => this.libraryIdentifier?.name;
}

class ExportDirectiveAnalysis
    extends NamespaceBasedDirectiveAnalysis<ExportDirective> {
  ExportDirectiveAnalysis(ExportDirective directive, SourceAnalysis source)
      : super(directive, source);
}

class ImportDirectiveAnalysis
    extends NamespaceBasedDirectiveAnalysis<ImportDirective> {
  late String prefix;
  ImportDirectiveAnalysis(ImportDirective directive, SourceAnalysis source)
      : super(directive, source) {
    this.prefix = directive.prefix?.name ?? "";
  }
}

class PartDirectiveAnalysis extends UriBasedDirectiveAnalysis<PartDirective> {
  PartDirectiveAnalysis(PartDirective directive, SourceAnalysis source)
      : super(directive, source);
}

class PartOfDirectiveAnalysis
    extends LibraryBasedDirectiveAnalysis<PartOfDirective> {
  PartOfDirectiveAnalysis(PartOfDirective directive, SourceAnalysis source)
      : super(directive, source, directive.libraryName);
}

class LibraryDirectiveAnalysis
    extends LibraryBasedDirectiveAnalysis<LibraryDirective> {
  LibraryDirectiveAnalysis(LibraryDirective directive, SourceAnalysis source)
      : super(directive, source, directive.name2);
}

// class TopLevelVarAnalysis extends EntityAnalysis<TopLevelVariableDeclaration,
//     TopLevelVariableFinder, VariableMirror> {
class TopLevelVarAnalysis extends EntityAnalysis<TopLevelVariableDeclaration,
    TopLevelVariableFinder> {
  var defaultValue;
  String? typeString;
  VariableDeclaration variableDeclaration;
  TypeAnnotation? typeAnalysis;

  TopLevelVarAnalysis.fromAnalysis(TopLevelVariableDeclaration topLevelVariable,
      this.variableDeclaration, SourceAnalysis source)
      : super.fromAnalysis(topLevelVariable, source) {
    this.name = variableDeclaration.name.value().toString();
    this.defaultValue =
        variableDeclaration.initializer?.accept(ConstantEvaluator());
    this.typeAnalysis = topLevelVariable.variables.type;
    if (typeAnalysis != null) {
      this.typeString = typeAnalysis?.type
              ?.getDisplayString(withNullability: false) ??
          (typeAnalysis.toString().isEmpty ? "var" : typeAnalysis.toString());
      if (typeAnalysis is NamedType &&
          (typeAnalysis as NamedType).typeArguments != null &&
          !typeString!.contains("<")) {
        this.typeString =
            "$typeString<${(typeAnalysis as NamedType).typeArguments?.arguments.join(",")}>";
      }
    } else {
      this.typeString = "var";
    }
  }

  Map toMap() => {
        "name": this.name,
        "docs": this.docs.isEmpty ? "" : "///$docs",
        "type": this.typeString,
        "typeString": this.typeString,
        "defaultValue": this.defaultValue,
        "metadata": this.metadata.map((m) => m.toMap()).toList(),
        "toString": this.toString()
      };

  String toString() => analyzerDeclaration.toString();
}

class MixinAnalysis {
  MixinDeclaration declaration;
  late SourceSpan location;
  // Map<String, FieldAnalysis> _fields = {};
  // Map<String, MethodAnalysis> _methods = {};

  MixinAnalysis.fromAnalysis(this.declaration,
      {required SourceAnalysis source}) {
    this.location = computeLocationFromNode(declaration, source);
    // declaration.members.forEach((ClassMember member) {
    //   if (member is FieldDeclaration) {
    //     member.fields.variables.forEach((VariableDeclaration declaration) {
    //       FieldAnalysis field =
    //           FieldAnalysis.fromAnalysis(this, member, declaration);
    //       this.fields[field.name] = field;
    //     });
    //   } else if (member is ConstructorDeclaration) {
    //     throw UnsupportedError("A constructor in a mixin???");
    //   } else if (member is MethodDeclaration) {
    //     MethodAnalysis analysis = MethodAnalysis.fromAnalysis(this, member);
    //     this.methods[analysis.name] = analysis;
    //   }
    // });
  }

  String get name => declaration.name.value().toString();
  Map<String, FieldAnalysis> get fields => throw UnimplementedError(
      "no me metí en el quilombo de hacer un ClassOrMixinAnalysis");
  Map<String, MethodAnalysis> get methods => throw UnimplementedError(
      "no me metí en el quilombo de hacer un ClassOrMixinAnalysis");
  // Map<String, FieldAnalysis> get fields => _fields;
  // Map<String, MethodAnalysis> get methods => _methods;
}

class EnumAnalysis {
  EnumDeclaration declaration;
  late SourceSpan location;

  EnumAnalysis.fromAnalysis(this.declaration,
      {required SourceAnalysis sourceAnalysis}) {
    this.location = computeLocationFromNode(declaration, sourceAnalysis);
  }

  String get name => declaration.name.value().toString();
  List<String> get simpleConstants =>
      declaration.constants.map((c) => c.name.value().toString()).toList();
}

// class ClassAnalysis
//     extends EntityAnalysis<ClassDeclaration, ClassFinder, ClassMirror> {
class ClassAnalysis extends EntityAnalysis<ClassDeclaration, ClassFinder> {
  ClassAnalysis? superclassAnalysis;
  String? superclassName;
  Map<String, FieldAnalysis> fields = {};
  Map<String, MethodAnalysis> methods = {};
  Map<String, ConstructorAnalysis> constructors = {};
  // InstanceMirror instanceMirror;

  // factory ClassAnalysis.fromInstance(Object instance) {
  //   InstanceMirror instanceMirror = reflect(instance);
  //   return ClassAnalysis.fromMirror(instanceMirror.type, instanceMirror);
  // }

  // factory ClassAnalysis.fromType(Type type) {
  //   ClassMirror mirror = reflectClass(type);
  //   return ClassAnalysis.fromMirror(mirror);
  // }

  // ClassAnalysis.fromMirror(ObjectMirror mirror, [this.instanceMirror])
  //     : super(mirror as ClassMirror) {
  //   ClassMirror classMirror = (mirror as ClassMirror);
  //   this.superclassAnalysis = classMirror.superclass == null ||
  //           classMirror.superclass.reflectedType == Object
  //       ? null
  //       : ClassAnalysis.fromMirror(classMirror.superclass, instanceMirror);
  //   this.superclassName = superclassAnalysis?.name;
  //   Map<String, FieldAnalysis> fields = {};
  //   Map<String, MethodAnalysis> methods = {};
  //   classMirror.declarations.forEach((Symbol name, DeclarationMirror d) {
  //     // if (d is VariableMirror) this.fields.add(FieldAnalysis.fromMirror(d));
  //     // if (d is MethodMirror) this.methods.add(MethodAnalysis.fromMirror(d));
  //     if (d is VariableMirror) {
  //       FieldAnalysis analysis = FieldAnalysis(this, d);
  //       fields[analysis.name] = analysis;
  //     }
  //     if (d is MethodMirror) {
  //       if (d.isConstructor) {
  //         var analysis = ConstructorAnalysis(this, d);
  //         this.constructors[analysis.name] = analysis;
  //       } else {
  //         var analysis = MethodAnalysis(this, d);
  //         methods[analysis.name] = analysis;
  //       }
  //     }
  //   });
  //   if (superclassAnalysis != null) {
  //     this.fields = Map.fromEntries(superclassAnalysis.fields.entries);
  //     this.methods = Map.fromEntries(superclassAnalysis.methods.entries);
  //   }
  //   this.fields.addAll(fields);
  //   this.methods.addAll(methods);
  // }

  ClassAnalysis.fromAnalysis(ClassDeclaration classDeclaration,
      {required SourceAnalysis source})
      : super.fromAnalysis(classDeclaration, source) {
    this.name = classDeclaration.name.value().toString();
    this.superclassName =
        classDeclaration.extendsClause?.superclass.name2.value().toString();
    classDeclaration.members.forEach((ClassMember member) {
      if (member is FieldDeclaration) {
        member.fields.variables.forEach((VariableDeclaration declaration) {
          FieldAnalysis field =
              FieldAnalysis.fromAnalysis(this, member, declaration);
          this.fields[field.name!] = field;
        });
      } else if (member is ConstructorDeclaration) {
        ConstructorAnalysis analysis =
            ConstructorAnalysis.fromAnalysis(this, member);
        this.constructors[analysis.name ?? ""] = analysis;
      } else if (member is MethodDeclaration) {
        MethodAnalysis analysis = MethodAnalysis.fromAnalysis(this, member);
        this.methods[analysis.name!] = analysis;
      }
    });
  }

  /// The name of the superclass present after "extends" clause, or `null` if none present
  String? get extend =>
      superclassAnalysis?.name ??
      analyzerDeclaration.extendsClause?.superclass.name2.value().toString();

  /// The interfaces that appears after the "implements" statement, or `null` if none present
  List<String> get interfaces =>
      analyzerDeclaration.implementsClause?.interfaces
          .map<String>((i) => i.name2.value().toString())
          .toList() ??
      [];

  /// The mixins that appears after the "with" statement, or `null` if none present
  List<String> get mixins =>
      analyzerDeclaration.withClause?.mixinTypes
          .map<String>((m) => m.name2.value().toString())
          .toList() ??
      [];

  ///Filter the methods that aren't getters or setters only
  Map<String, MethodAnalysis> get regularMethods =>
      Map.fromEntries(methods.entries.where((m) =>
          // m.value?.mirror?.isRegularMethod ??
          (m.value.analyzerDeclaration.isSetter &&
              m.value.analyzerDeclaration.isGetter) ==
          false));

  ///Filter the getter methods only
  Map<String, MethodAnalysis> get getters => Map.fromEntries(
      methods.entries.where((m) => m.value.analyzerDeclaration.isGetter));
  // m.value?.mirror?.isGetter ?? m.value.analyzerDeclaration.isGetter));

  ///Filter the setter methods only
  Map<String, MethodAnalysis> get setters => Map.fromEntries(
      methods.entries.where((m) => m.value.analyzerDeclaration.isSetter));
  // m.value?.mirror?.isSetter ?? m.value.analyzerDeclaration.isSetter));

  ///Filter the static methods only
  Map<String, MethodAnalysis> get staticMethods => Map.fromEntries(
      methods.entries.where((m) => m.value.analyzerDeclaration.isStatic));
  // m.value?.mirror?.isStatic ?? m.value.analyzerDeclaration.isStatic));

  // bool get isEnum => this?.mirror?.isEnum;

  Map toMap() => {
        "name": this.name,
        "docs": this.docs.isEmpty ? "" : "///$docs",
        "metadata": this.metadata.map((m) => m.toMap()).toList(),
        "fields": this.fields.values.map((v) => v.toMap()).toList(),
        "getters": this.getters.values.map((v) => v.toMap()).toList(),
        "setters": this.setters.values.map((v) => v.toMap()).toList(),
        "methods": this.methods.values.map((v) => v.toMap()).toList(),
        "constructors": this.constructors.values.map((v) => v.toMap()).toList(),
      };
}

class ClassMemberAnalysis<D extends ClassMember, F extends Finder>
    extends EntityAnalysis<D, F> {
  ClassAnalysis container;
  // ClassMemberAnalysis(
  //     DeclarationMirror mirror, this.container, SourceAnalysis sourceAnalysis)
  //     : super(mirror,
  //           analyzerContainer: container.analyzerDeclaration,
  //           source: sourceAnalysis);

  ClassMemberAnalysis.fromAnalysis(
      D declaration, this.container, SourceAnalysis sourceAnalysis)
      : super.fromAnalysis(declaration, sourceAnalysis);
}

/// Analyzes the variables declarations inside classes scope (fields)
/// The [FieldDeclaration]s have the vissicitude of being able to be declared
/// with several variables in a single field (for example: 'Type var1 = val1,
/// var2' will have 2 [VariableDeclaration]s declared in the same field.
/// While the [VariableMirror] doesn't do that distinction at all. In my
/// implementation of this [FieldAnalysis] class, the `analyzerDeclaration` will
/// hold the field, while `variableDeclaration` the variable
class FieldAnalysis extends ClassMemberAnalysis<FieldDeclaration, FieldFinder> {
  // extends ClassMemberAnalysis<FieldDeclaration, FieldFinder, VariableMirror> {
  // TypeMirror typeMirror;
  TypeAnalysis? type;
  VariableDeclaration variableDeclaration;

  // bool _hasInstanceMirror;
  // ClassMirror _classMirror;

  // ClassMirror get classMirror {
  //   _classMirror ??= reflectClass(typeMirror.reflectedType);
  //   return _classMirror;
  // }

  // /// If there's an instance mirror, get that instance's this field value and
  // /// return a mirror on that result
  // InstanceMirror get getInstanceMirror {
  //   if (_hasInstanceMirror) {
  //     return container.instanceMirror.getField(Symbol(this.name));
  //   } else {
  //     throw StateError(
  //         "Requesting instanceMirror of a field whose ClassAnalysis's "
  //         "parent container was declared without .fromInstance constructor");
  //   }
  // }

  // /// If there's an instance mirror, set that instance's this field value and
  // /// return a mirror on the result
  // InstanceMirror setInstanceMirror(value) {
  //   if (_hasInstanceMirror) {
  //     return container.instanceMirror.setField(Symbol(this.name), value);
  //   } else {
  //     throw StateError(
  //         "Requesting instanceMirror of a field whose ClassAnalysis's "
  //         "parent container was declared without .fromInstance constructor");
  //   }
  // }

  bool get isFinal => this.variableDeclaration.isFinal;
  bool get isConst => this.variableDeclaration.isConst;
  bool get isPrivate => this.name.startsWith("_");
  // bool get isPrivate => this.mirror?.isPrivate ?? this.name.startsWith("_");

  FieldAnalysis.fromAnalysis(ClassAnalysis container,
      FieldDeclaration fieldDeclaration, this.variableDeclaration)
      : super.fromAnalysis(fieldDeclaration, container, container.source) {
    this.name = variableDeclaration.name.value().toString();
    if (analyzerDeclaration.fields.type != null) {
      this.type = TypeAnalysis.fromAnalyzer(analyzerDeclaration.fields.type!);
    }
    // this._hasInstanceMirror = container.instanceMirror != null;
  }

  // FieldAnalysis(ClassAnalysis container, VariableMirror declaration)
  //     : super(declaration, container, container.source) {
  //   this.variableDeclaration = analyzerDeclaration?.fields?.variables
  //       ?.singleWhere(
  //           (VariableDeclaration d) => d.name.toString() == this.name);
  //   this.typeMirror = declaration.type;
  //   this.type =
  //       TypeAnalysis(declaration.type, analyzerDeclaration?.fields?.type);
  //   this._hasInstanceMirror = container.instanceMirror != null;
  // }

  get defaultValue {
    return variableDeclaration.initializer?.accept(DefaultValueVisitor());
  }

  Map toMap() => {
        "name": this.name,
        "docs": this.docs.isEmpty ? "" : "///$docs",
        "defaultValue": this.defaultValue,
        "hasDefaultValue": this.defaultValue != null,
        "type": this.type?.toMap(),
        "typeString": this.type.toString(),
        "toString": this.toString()
      };

  String toString() => analyzerDeclaration.toString();
}

class DefaultValueVisitor extends ConstantEvaluator {
  ///Usado generalmente en valores de enums, en cuyo caso devuelve el enum pasado a String
  String visitPrefixedIdentifier(PrefixedIdentifier node) {
    return node.prefix.name + "." + node.identifier.name;
  }
}

class TypeAnalysis {
  late String name;
  List<String> arguments = [];
  TypeAnnotation analyzerDeclaration;
  // TypeMirror _mirror;

  // factory TypeAnalysis(TypeMirror mirror, TypeAnnotation analyzerDeclaration) {
  //   // if (analyzerDeclaration == null)
  //   //   throw ArgumentError.notNull("analyzerDeclaration");
  //   var ret = TypeAnalysis.fromAnalyzer(analyzerDeclaration);
  //   ret._mirror = mirror;
  //   return ret;
  // }

  TypeAnalysis.fromAnalyzer(this.analyzerDeclaration) {
    if (analyzerDeclaration is NamedType) {
      this.name = (analyzerDeclaration as NamedType).name2.value().toString();
      this.arguments = (analyzerDeclaration as NamedType)
              .typeArguments
              ?.arguments
              .map<String>((a) => (a as NamedType).name2.value().toString())
              .toList() ??
          [];
    } else if (analyzerDeclaration is GenericFunctionType) {
      this.name = (analyzerDeclaration as GenericFunctionType)
              .returnType
              ?.type
              ?.getDisplayString(withNullability: true) ??
          "dynamic";
      this.arguments =
          ((analyzerDeclaration as GenericFunctionType).returnType as NamedType)
                  .typeArguments
                  ?.arguments
                  .map((a) => (a as NamedType).name2.value().toString())
                  .toList() ??
              [];
    }
  }

  // TypeMirror get mirror {
  //   if (_mirror == null) {
  //     throw StateError("Requesting a mirror for type generated "
  //         "through package:analyzer analysis, not from "
  //         "mirroring an instance");
  //   } else {
  //     return _mirror;
  //   }
  // }

  Map toMap() {
    Map ret = {"name": this.name, "toString": toString()};
    for (int i = 0; i < 4; i++) {
      ret["argument$i"] = (i < arguments.length) ? arguments[i] : "dynamic";
    }
    return ret;
  }

  String toString() => analyzerDeclaration.toString();
}

// class ConstructorAnalysis extends ClassMemberAnalysis<ConstructorDeclaration,
//     ConstructorFinder, MethodMirror> with ParametersInterface {
class ConstructorAnalysis
    extends ClassMemberAnalysis<ConstructorDeclaration, ConstructorFinder>
    with ParametersInterface {
  // ConstructorAnalysis(ClassAnalysis container, MethodMirror declaration)
  //     : super(declaration, container, container.source) {
  //   if (declaration.parameters.isNotEmpty)
  //     this.parameters = ParametersAnalysis(this, declaration);
  // }

  ConstructorAnalysis.fromAnalysis(
      ClassAnalysis classAnalysis, ConstructorDeclaration declaration)
      : super.fromAnalysis(declaration, classAnalysis, classAnalysis.source) {
    if (analyzerDeclaration.parameters.parameters.isNotEmpty) {
      this.parameters =
          ParametersAnalysis.fromAnalysis(this, declaration.parameters);
    }
  }

  bool get isConst => analyzerDeclaration.constKeyword != null;
  bool get isExternal => analyzerDeclaration.externalKeyword != null;
  bool get isFactory => analyzerDeclaration.factoryKeyword != null;
  // bool get isPrivate => mirror.isPrivate;

  // _computeName() {
  //   String str = MirrorSystem.getName(this.mirror.simpleName);
  //   int index = str.indexOf(r".");
  //   this.name = index == -1 ? "" : str.substring(index + 1);
  // }

  Map toMap() => {
        "name": this.name,
        "docs": this.docs.isEmpty ? "" : "///$docs",
        "parameters": this.parameters?.all.map((p) => p.toMap()).toList() ?? [],
        "metadata": this.metadata.map((m) => m.toMap()).toList(),
        "toString": this.toString()
      };

  String toString() => analyzerDeclaration.toString();
}

// class MethodAnalysis
//     extends ClassMemberAnalysis<MethodDeclaration, MethodFinder, MethodMirror>
class MethodAnalysis
    extends ClassMemberAnalysis<MethodDeclaration, MethodFinder>
    with ParametersInterface {
  late TypeAnalysis returnType;

  // MethodAnalysis(ClassAnalysis container, MethodMirror declaration)
  //     : super(declaration, container, container.source) {
  //   if (declaration.parameters.isNotEmpty) {
  //     this.parameters = ParametersAnalysis(this, declaration);
  //   }
  //   this.name = MirrorSystem.getName(declaration.simpleName);

  //   this.returnType =
  //       TypeAnalysis(declaration.returnType, analyzerDeclaration?.returnType);
  // }

  MethodAnalysis.fromAnalysis(
      ClassAnalysis classAnalysis, MethodDeclaration member)
      : super.fromAnalysis(member, classAnalysis, classAnalysis.source) {
    if (member.parameters != null) {
      this.parameters =
          ParametersAnalysis.fromAnalysis(this, member.parameters!);
    }
    this.name = member.name.value().toString();
    if (analyzerDeclaration.returnType != null) {
      this.returnType =
          TypeAnalysis.fromAnalyzer(analyzerDeclaration.returnType!);
    }
  }

  // bool get isPrivate => mirror?.isPrivate ?? this.name.startsWith("_");
  bool get isPrivate => this.name.startsWith("_");

  // InstanceMirror invocationMirror(
  //     {List positionalArguments,
  //     Map<Symbol, dynamic> namedArguments,
  //     setterValue}) {
  //   if (container.instanceMirror == null) {
  //     throw StateError(
  //         "Requesting instanceMirror of a method whose ClassAnalysis's "
  //         "parent container was declared without .fromInstance constructor");
  //   } else {
  //     if (mirror.isGetter) {
  //       return container.instanceMirror.getField(Symbol(this.name));
  //     } else if (mirror.isSetter) {
  //       return container.instanceMirror
  //           .setField(Symbol(this.name), setterValue);
  //     } else {
  //       return container.instanceMirror
  //           .invoke(Symbol(name), positionalArguments, namedArguments);
  //     }
  //   }
  // }

  String get returnTypeString => returnType.toString();

  Map toMap() => {
        "name": this.name,
        "docs": this.docs.isEmpty ? "" : "///$docs",
        "metadata": this.metadata.map((m) => m.toMap()).toList(),
        "parameters": this.parameters?.all.map((p) => p.toMap()).toList(),
        "returnType": this.returnType.toMap(),
        "returnTypeString": this.returnTypeString,
        "toString": this.toString()
      };

  String toString() => analyzerDeclaration.toString();
}

class ParametersInterface {
  ParametersAnalysis? parameters;

  /// Syntactic sugar for the set of always optional parameters that need a name
  /// to be invoked in arguments
  Set<Parameter> get namedParameters => parameters?.named ?? {};

  /// Syntactic sugar for the always required, ordered, positional parameters
  List<Parameter> get requiredParameters => parameters?.ordinary ?? [];

  /// Syntactic sugar for the optionals (both named as positional) parameters
  List<Parameter> get optionalParameters => parameters?.optionals ?? [];

  /// Same as `requiredParameters`, the normal old school lifelonging parameters
  List<Parameter> get ordinaryParameters => parameters?.ordinary ?? [];

  /// Syntactic sugar for the List of parameters that should (or could) be
  /// invoked (both the types of required and the optionals)
  List<Parameter> get positionalParameters => parameters?.positionals ?? [];

  /// Syntactic sugar for the List of positional optional parameters (those
  /// defined between square brackets)
  List<Parameter> get positionalOptionalParameters =>
      parameters?.positionalOptionals ?? [];
}

class Parameter {
  dynamic defaultValue;
  late Type type;
  String? typeString;
  late String name;
  // ParameterMirror mirror;
  FormalParameter node;

  // Parameter(FormalParameterList container, this.mirror) {
  //   this.type = mirror.type.reflectedType;
  //   this.name = MirrorSystem.getName(mirror.simpleName);
  //   this.defaultValue =
  //       mirror.hasDefaultValue ? mirror.defaultValue.reflectee : null;
  //   try {
  //     this.node = container.parameters.singleWhere(
  //         (FormalParameter parameter) =>
  //             parameter.identifier.toString() == this.name);
  //   } catch (e) {
  //     throw Exception(
  //         "Weird error... there seems to not exist a parameter node for a mirror of itself");
  //   }
  // }

  Parameter.fromAnalysis(this.node) {
    this.name = node.name!.value().toString();
    if (node is DefaultFormalParameter) {
      this.defaultValue = (node as DefaultFormalParameter)
          .defaultValue
          ?.accept(ConstantEvaluator());
      if (defaultValue == NOT_A_CONSTANT) {
        this.defaultValue = null;
      }
      this.node = (node as DefaultFormalParameter).parameter;
    }
    if (node is SimpleFormalParameter) {
      this.typeString = (node as SimpleFormalParameter)?.type?.toString();
    } else if (node is FieldFormalParameter) {
      this.typeString = (node as FieldFormalParameter)
          .type
          ?.type
          ?.getDisplayString(withNullability: false);
    } else {
      throw UnimplementedError("No programé qué hacer con ${node.runtimeType}");
    }
  }

  bool get isOptional => node.isOptional;
  bool get isNamed => node.isNamed;
  bool get isOrdinary => node.isRequired;
  bool get isPositionalOptional => node.isOptionalPositional;
  bool get isRequired =>
      node.isRequired || node.metadata.any((m) => m.name.name == "required");
  bool get isThisInitializer => node is FieldFormalParameter;
  // bool get isThisInitializer => node.beginToken.toString() == "this";

  String get docs =>
      (this.node as NormalFormalParameter).documentationComment.toString();

  List<Annotation> get metadata => node.metadata.toList();

  Map toMap() => {
        "name": this.name,
        "docs": this.docs.isEmpty ? "" : "///$docs",
        "type": this.typeString,
        "metadata": this.metadata.map((m) => m.toString()).toList(),
        "toString": this.toString()
      };

  String toString() => node.toString();
}

// class ParametersAnalysis
//     extends EntityAnalysis<FormalParameterList, ParameterFinder, MethodMirror> {
class ParametersAnalysis
    extends EntityAnalysis<FormalParameterList, ParameterFinder> {
  ClassMemberAnalysis container;
  List<Parameter> _parameters = [];

  // ParametersAnalysis(this.container, MethodMirror mirror)
  //     : super(mirror,
  //           source: container.source,
  //           analyzerContainer: container.analyzerDeclaration) {
  //   if (analyzerDeclaration != null) {
  //     mirror.parameters.forEach((ParameterMirror m) {
  //       this._parameters.add(Parameter(analyzerDeclaration, m));
  //     });
  //   }
  // }

  ParametersAnalysis.fromAnalysis(
      this.container, FormalParameterList parameters)
      : super.fromAnalysis(parameters, container.source) {
    this.analyzerDeclaration?.parameters?.forEach((FormalParameter param) {
      this._parameters.add(Parameter.fromAnalysis(param));
    });
  }

  /// All the parameters, simple as they are
  List<Parameter> get all => _parameters;

  /// Always required, ordered, positional parameters
  List<Parameter> get required =>
      _parameters.where((p) => p.isOrdinary).toList();

  /// Optional parameters (both positional and named)
  List<Parameter> get optionals =>
      _parameters.where((p) => p.isOptional).toList();

  /// Set of always optional parameters that need a name to be invoked in
  /// arguments
  Set<Parameter> get named => _parameters.where((p) => p.isNamed).toSet();

  /// The normal old school lifelonging parameters (required positionals)
  List<Parameter> get ordinary =>
      _parameters.where((p) => p.isOrdinary).toList();

  /// List of parameters that should (or could) be invoked in a stablished order
  /// (both the required and the optionals types)
  List<Parameter> get positionals =>
      _parameters.where((p) => p.isOrdinary || p.isPositionalOptional).toList();

  /// The positional optional parameters (those defined betwenn square brackets)
  List<Parameter> get positionalOptionals =>
      _parameters.where((p) => p.isPositionalOptional).toList();

  get length => _parameters.length;

  operator [](String name) =>
      _parameters.singleWhere((Parameter p) => p.name == name, orElse: null);
}
